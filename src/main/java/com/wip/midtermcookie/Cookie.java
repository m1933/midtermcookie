/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.midtermcookie;

/**
 *
 * @author WIP
 */
public class Cookie {

    private String name;
    private int q = 5;
    private char flav = 'C';
    private String flavour;

    public Cookie(String name, int quantity, char flavour) {
        this.name = name;
        if (quantity > 0) {
            this.q = quantity;
        } 
        if (flavour != '-') {
            this.flav = flavour;
        } 
        
    }

    public String getName() {
        return name;
    }

    public int getQ() {
        return q;
    }

    public char getFlav() {
        return flav;
    }

    public String getFlavour() {
        return flavour;
    }

    public String Name(String name) {
        return this.name = name;
    }

    public int Quantity(int quantity) {
        return this.q = quantity;
    }

    public char Flavour(char flav) {
        return this.flav = flav;
    }

    @Override
    public String toString() {
        switch (this.flav) {
            case 'C':
                flavour = "Chocolate";
                break;
            case 'S':
                flavour = "Strawberry";
                break;
            case 'M':
                flavour = "Matcha";
                break;
            default:
                break;
        }
        return "name: " + "'" + this.name + "'" + " & " + this.q + " pieces of " + flavour;
    }
}
