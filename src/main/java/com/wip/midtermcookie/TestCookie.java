/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.midtermcookie;

/**
 *
 * @author WIP
 */
public class TestCookie {
    public static void main(String[] args) {
        //Round1
        Cookie custom1 = new Cookie("NeedCookie", 7, 'S');
        System.out.println(custom1.toString());
        
        Cookie custom2 = new Cookie("Chocky", 3, 'C');
        System.out.println(custom2.toString());
        
        Cookie custom3 = new Cookie("ChaGreenLover", 0, 'M');
        System.out.println(custom3.toString());
        
        Cookie custom4 = new Cookie("BaanKanom", 6, '-');
        System.out.println(custom4.toString());
        
        System.out.println("");
        //Round2
        custom1.Quantity(10);
        System.out.println(custom1.toString());
        
        custom2.Name("Chockylate");
        System.out.println(custom2.toString());
        
        custom4.Flavour('M');
        System.out.println(custom4.toString());
        
        custom3.Name("ChaGreen");
        custom3.Quantity(8);
        System.out.println(custom3.toString());
    }
}
